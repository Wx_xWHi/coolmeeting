## 补充完成[SSM实战项目之会议管理系统（2020版）](https://www.bilibili.com/video/BV1ep4y1S7s6?from=search&seid=15741768683205412666)
### 密码都是1  普通账号wangxh  管理员账号linyk
### searchmeeting页面->搜索会议查询功能
### notifications通知页面->未来7天我要参加的会议: 不搜索已经取消的会议
### mybookings变为修改我的预定->不搜索已经取消的预定会议
### 补充修改密码功能
### 补充退出功能->清除名为currentuser的session

### 避坑
* 因为如果数据为null, freemarker没有使用${meeting.roomid!""}中 的 !"" 会报错

### 自我反省:
* 想到就写,没打草稿,方法名是中式英语,且忘记了原作者配置了AOP的事务,对add*,insert*,update*,delete方法名开的是方法进行拦截并开启事务.
* 要有数据字典不然懵逼0,1,2是什么..写Sql的where就很烦了
meeting.status=1 是取消了的会议 0 正常的会议 
employee.role=2 是管理员  1是正常
employee.status=0 待通过 1通过 2不通过

### 前端笔记
* `<a href="/"> 加了"/"是绝对路径,没加是相对路径,在登录admin后,会转到/admin/**,此时若href没"/",就自动添加"/admin",,导致没有@Requestmapper("")映射处理`
* `<a class="clickbutton">链接变成按钮`
* `<form></form>`中的标签通过 name="" 就是提交到服务器的参数的参数名
* freemarker中 {{meeeting!""}}中!"" 避免meeting数据为null报错
* freemakrer from 时间问题 ${cm.starttime?string('yyyy-MM-dd HH:mm:ss')}
* Model中 @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")  starttime
* 实现convert 配置时间转换器
+ forward是服务器内部转发 
+ redirect是重定向让客户端在访问目标
* 本来Aop时设置了  add*,insert*,update*,delet* 事务,但补充完成时好像用来do.来命名..


### 通过DTO补充或删减Model来提供给前端使用
```java

    public List<CancelMeeting> getCancelMeeting() {

        List<Meeting> list = meetingMapper.getCancelMeeting();
        List<RoomDTO> listroomDTO = meetingRoomMapper.getAll();

        //制作映射{RoomId:RoomName}的集合resultMap   方便DTO补充 通过map.get(key)获取value
        Map<Integer, String> resultMap = listroomDTO.stream().collect(Collectors.toMap(
                RoomDTO::getRoomId, RoomDTO::getRoomName
        ));

        //完成DTO复制Model,补充属性
        List<CancelMeeting> listDto = list.stream().map(meeting -> {

            CancelMeeting cancelMeeting = new CancelMeeting();

            BeanUtils.copyProperties(meeting, cancelMeeting);

            cancelMeeting.setRoomname(resultMap.get(meeting.getRoomid()));

            return cancelMeeting;
        }).collect(Collectors.toList());
        return listDto;
    }

```


### mybatis笔记
* 对于Model中有集合元素的,列表元素用<collection>被<resultMap extends='BaseResultMap' id='',type=''>包裹完成拓展
* Mapper接口中方法参数通过@Param("A")User user //参数是对象,在.xml中可以通过#{A.属性名}. <if test="A.employeename!=null">
* 参数是列表List<Integer> list ,xml中可以
```xml
where id in
<foreach collection="list" index="index" item="item" open="(" separator="," close=")">
	#{item}
 </foreach>
```
* select  表名.列名 as 属性名 可以完成映射 (一般resultMap="包名.某DTO")
* and 表名.列名  不可以用上面的属性名替代
* insert id自增加useGeneratedKeys="true" keyProperty="meetingid"

## JS Ajax笔记  
##### js注释 时间插件 My97DatePicker 看bookmeeting.ftl  searchmeetings.ftl 有注解理解JS

