package org.javaboy.meeting.controller;

import org.javaboy.meeting.dto.MeetingDTO;
import org.javaboy.meeting.model.Employee;
import org.javaboy.meeting.model.Meeting;
import org.javaboy.meeting.service.EmployeeService;
import org.javaboy.meeting.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 21:57
 */
@Controller
public class ChangeBookingController {

    @Autowired
    MeetingService meetingService;

    @Autowired
    EmployeeService employeeService;


    @RequestMapping("/mybookings")
    public String mybookings(HttpSession httpSession, Model model) {
        Employee currentuser = (Employee) httpSession.getAttribute("currentuser");
        Integer employeeid = currentuser.getEmployeeid();
        List<MeetingDTO> list = meetingService.getmeetingofmybookCanCancle(employeeid);

        model.addAttribute("mlist", list);

        return "mybookings";
    }

    @RequestMapping("/mymeetingdetails")
    public String meetingdetails(Integer meetingid, Model model) {
        Meeting meeting = meetingService.getmeetingByid(meetingid);
        model.addAttribute("meeting", meeting);
        List<Employee> listEmployee = employeeService.getEmpsByid(meetingid);
        model.addAttribute("ems", listEmployee);
        return "mymeetingdetails";
    }

    @GetMapping("/cancelmeeting")
    public String cancelmeeting(@RequestParam(name = "meetingid") Integer meetingid,
                                @RequestParam(name = "meetingname") String meetingname, Model model) {
        model.addAttribute("meetingid", meetingid);
        model.addAttribute("meetingname", meetingname);

        return "cancelmeeting";
    }

    @PostMapping("/dpCancel")
    public String doCancel(Integer meetingid, String canceledreason) {
        meetingService.cancelmeeting(meetingid, canceledreason);

        return "forward:mybookings";
    }
}
