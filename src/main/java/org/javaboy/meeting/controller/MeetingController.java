package org.javaboy.meeting.controller;

import org.javaboy.meeting.dto.MeetingDTO;
import org.javaboy.meeting.model.Department;
import org.javaboy.meeting.model.Employee;
import org.javaboy.meeting.model.Meeting;
import org.javaboy.meeting.service.DepartmentService;
import org.javaboy.meeting.service.EmployeeService;
import org.javaboy.meeting.service.MeetingRoomService;
import org.javaboy.meeting.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/2 17:47
 */
@Controller
public class MeetingController {
    public static final Integer PAGE_SIZE = 10;

    @Autowired
    MeetingRoomService meetingRoomService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    MeetingService meetingService;

    @RequestMapping("/bookmeeting")
    public String bookmeeting(Model model) {
        model.addAttribute("mrs", meetingRoomService.getAllMr());
        return "bookmeeting";

    }

    @RequestMapping("/alldeps")
    @ResponseBody
    public List<Department> getAllDeps() {
        return departmentService.getAllDeps();
    }

    @RequestMapping("/getempbydepid")
    @ResponseBody
    public List<Employee> getEmpsByDepId(Integer depId) {
        return employeeService.getEmpsByDepId(depId);
    }

    @RequestMapping("/doAddMeeting")
    public String doAddMeeting(Meeting meeting, Integer[] mps, HttpSession session) {
        Employee currentuser = (Employee) session.getAttribute("currentuser");
        meeting.setReservationistid(currentuser.getEmployeeid());
        Integer result = meetingService.addMeeting(meeting, mps);
        if (result == 1) {

            return "redirect:/searchmeetings";
        } else {
            return "forward:/bookmeeting";
        }
    }

    @RequestMapping("/searchmeetings")
    public String getAllMeetings(MeetingDTO meetingDTO, @RequestParam(defaultValue = "1") Integer page, Model model) {
        List<MeetingDTO> listMeetingDTOs = meetingService.listMeetingDTOs(meetingDTO, page, PAGE_SIZE);

        Long total = meetingService.getTotal(meetingDTO);

        model.addAttribute("meetings", listMeetingDTOs);
        model.addAttribute("total", total);
        model.addAttribute("page", page);
        model.addAttribute("pagenum", total % PAGE_SIZE == 0 ? total / PAGE_SIZE : total / PAGE_SIZE + 1);

        return "searchmeetings";
    }

    @RequestMapping("/meetingdetails")
    public String meetingdetails(@RequestParam(name = "meetingid") Integer meetingid, Model model) {
        Meeting meeting = meetingService.getmeetingByid(meetingid);
        model.addAttribute("meeting", meeting);
        List<Employee> listEmployee = employeeService.getEmpsByid(meetingid);
        model.addAttribute("ems", listEmployee);
        return "meetingdetails";
    }

}
