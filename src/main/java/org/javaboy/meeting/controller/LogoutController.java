package org.javaboy.meeting.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 19:45
 */
@Controller
public class LogoutController {
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        //将session从页面中移除
        request.getSession().removeAttribute("currentuser");

        return "redirect:/";
    }


}
