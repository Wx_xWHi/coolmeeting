package org.javaboy.meeting.controller;

import org.javaboy.meeting.model.Employee;
import org.javaboy.meeting.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 21:36
 */
@Controller
public class ChangePasswordController {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping("/changepassword")
    public String changehtml() {
        return "changepassword";
    }

    @PostMapping("/dochange")
    public String dochang(String username, String password, String newpassword, Model model) {
        Employee employee = employeeService.doLogin(username, password);
        if (employee == null) {
            model.addAttribute("error", "用户名或原密码输入错误，修改失败");
            return "forward:changepassword";
        } else {
            employeeService.doChang(username, newpassword);
        }
        return "redirect:/";
    }
}
