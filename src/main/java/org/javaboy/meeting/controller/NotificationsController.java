package org.javaboy.meeting.controller;

import org.javaboy.meeting.model.Employee;
import org.javaboy.meeting.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/8/31 8:35
 */
@Controller
public class NotificationsController {

    @Autowired
    MeetingService meetingService;


    @GetMapping("/notifications")
    public String Notifications(Model model, HttpSession session) {

        Employee currentuser = (Employee) session.getAttribute("currentuser");
        Integer employeeid = currentuser.getEmployeeid();


        model.addAttribute("ms", meetingService.getSevenDayMeeting(employeeid));

        model.addAttribute("cms", meetingService.getCancelMeeting());

        return "notifications";
    }
}
