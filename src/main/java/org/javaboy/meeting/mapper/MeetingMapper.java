package org.javaboy.meeting.mapper;

import org.apache.ibatis.annotations.Param;
import org.javaboy.meeting.dto.MeetingDTO;
import org.javaboy.meeting.model.Meeting;

import java.util.List;

public interface MeetingMapper {

    Integer addMeeting(Meeting meeting);

    void addParticipants(@Param("meetingid") Integer meetingid, @Param("mps") Integer[] mps);


    List<MeetingDTO> listMeetingDTOs(@Param("mdto") MeetingDTO meetingDTO, @Param("page") Integer page,
                                     @Param("pagesize") Integer pagesize);

    Long getTotal(@Param("mdto") MeetingDTO meetingDTO);


    List<Meeting> getMeetingById(Integer employeeid);

    List<Meeting> getCancelMeeting();

    Meeting getmeetingByid(Integer meetingid);

    List<MeetingDTO> getmeetingofmybook(Integer employeeid);

    void cancelmeeting(Integer meetingid, String canceledreason);

}