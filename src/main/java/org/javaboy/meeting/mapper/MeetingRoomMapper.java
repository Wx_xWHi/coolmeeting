package org.javaboy.meeting.mapper;

import org.javaboy.meeting.dto.RoomDTO;
import org.javaboy.meeting.model.MeetingRoom;

import java.util.List;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/1 15:48
 */
public interface MeetingRoomMapper {
    List<MeetingRoom> getAllMr();

    MeetingRoom getMrById(Integer roomid);

    Integer updateRoom(MeetingRoom meetingRoom);

    Integer addMr(MeetingRoom meetingRoom);


    List<RoomDTO> getAll();

}
