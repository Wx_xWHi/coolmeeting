package org.javaboy.meeting.mapper;

import java.util.List;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 19:31
 */
public interface MeetingParticipantsMapper {

    List<Integer> getAllBymeetingid(Integer meetingid);

}
