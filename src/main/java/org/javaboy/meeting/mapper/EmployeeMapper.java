package org.javaboy.meeting.mapper;

import org.apache.ibatis.annotations.Param;
import org.javaboy.meeting.model.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeMapper {


    Employee loadEmpByUsername(String username);

    Integer doReg(Employee employee);

    List<Employee> getAllEmpsByStatus(Integer status);

    Integer approveaccount(@Param("employeeid") Integer employeeid, @Param("status") Integer status);


    List<Employee> getAllEmps(@Param("emp") Employee employee, @Param("page") Integer page,
                              @Param("pagesize") Integer pageSize);


    Long getTotal(Employee employee);


    List<Employee> getEmpsByDepId(Integer depId);

    Map<Integer, String> getIdAndName();

    List<Employee> getAllEmpsByid(List<Integer> list);

    void doChang(String username, String password);
}