package org.javaboy.meeting.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 14:27
 */
public class CancelMeeting {

    private String meetingname;
    private String roomname;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date starttime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endtime;
    private String canceledreason;
    private Integer roomid;
    private Integer meetingid;

    @Override
    public String toString() {
        return "CancelMeeting{" +
                "meetingname='" + meetingname + '\'' +
                ", roomname='" + roomname + '\'' +
                ", starttime=" + starttime +
                ", endtime=" + endtime +
                ", canceledreason='" + canceledreason + '\'' +
                ", roomid=" + roomid +
                ", meetingid=" + meetingid +
                '}';
    }

    public String getMeetingname() {
        return meetingname;
    }

    public void setMeetingname(String meetingname) {
        this.meetingname = meetingname;
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getCanceledreason() {
        return canceledreason;
    }

    public void setCanceledreason(String canceledreason) {
        this.canceledreason = canceledreason;
    }

    public Integer getRoomid() {
        return roomid;
    }

    public void setRoomid(Integer roomid) {
        this.roomid = roomid;
    }

    public Integer getMeetingid() {
        return meetingid;
    }

    public void setMeetingid(Integer meetingid) {
        this.meetingid = meetingid;
    }

}
