package org.javaboy.meeting.dto;

/**
 * TODO:
 *
 * @Author: 麦健豪
 * @Date: 2020/9/6 15:42
 */
public class RoomDTO {
    private Integer roomId;
    private String roomName;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
