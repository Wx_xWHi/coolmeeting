package org.javaboy.meeting.model;

import java.io.Serializable;

/**
 * department
 *
 * @author
 */
public class Department implements Serializable {

    private static final long serialVersionUID = 7709034482934654145L;
    private Integer departmentid;
    private String departmentname;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(Integer departmentid) {
        this.departmentid = departmentid;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentid=" + departmentid +
                ", departmentname='" + departmentname + '\'' +
                '}';
    }
}