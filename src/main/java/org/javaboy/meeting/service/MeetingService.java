package org.javaboy.meeting.service;

import org.javaboy.meeting.dto.CancelMeeting;
import org.javaboy.meeting.dto.MeetingDTO;
import org.javaboy.meeting.dto.RoomDTO;
import org.javaboy.meeting.dto.SevenDayMeeting;
import org.javaboy.meeting.mapper.MeetingMapper;
import org.javaboy.meeting.mapper.MeetingRoomMapper;
import org.javaboy.meeting.model.Meeting;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * TODO:
 *
 * @author  : 麦健豪
 * @date: 2020/9/2 22:00
 */
@Service
public class MeetingService {

    @Autowired
    MeetingMapper meetingMapper;

    @Autowired
    MeetingRoomMapper meetingRoomMapper;

    private List<Meeting> cancelMeeting;

    public List<MeetingDTO> listMeetingDTOs(MeetingDTO meetingDTO, Integer page, Integer pagesize) {
        page = (page - 1) * pagesize;
        return meetingMapper.listMeetingDTOs(meetingDTO, page, pagesize);
    }

    public Integer addMeeting(Meeting meeting, Integer[] mps) {
        meeting.setReservationtime(new Date());
        Integer result = meetingMapper.addMeeting(meeting);
        meetingMapper.addParticipants(meeting.getMeetingid(), mps);
        return result;
    }


    public Long getTotal(MeetingDTO meetingDTO) {
        return meetingMapper.getTotal(meetingDTO);
    }

    public List<SevenDayMeeting> getSevenDayMeeting(Integer employeeid) {

        List<Meeting> list = meetingMapper.getMeetingById(employeeid);
        List<RoomDTO> listroomDTO = meetingRoomMapper.getAll();
        //
        Calendar c = Calendar.getInstance();
        Date now = new Date();
        c.setTime(now);
        c.add(Calendar.DATE, 7);
        Date after7day = c.getTime();

        //过滤 得到未来7天内需要我的会议
        List<Meeting> listNext7Day =
                list.stream().filter(s ->
                        s.getStarttime().getTime() >= now.getTime() &&
                                s.getStarttime().getTime() <= after7day.getTime())
                        .collect(Collectors.toList());

        //得到Map<roomid,roomname>方便下面转换,添加对应的roomname
        Map<Integer, String> resultMap = listroomDTO.stream().collect(Collectors.toMap(
                RoomDTO::getRoomId, RoomDTO::getRoomName
        ));
        //转换Meeting为SevenDayMeeting,更好的在View展示,补充Meeting缺失的属性
        List<SevenDayMeeting> listDto = listNext7Day.stream().map(meeting -> {

            SevenDayMeeting sevenDayMeeting = new SevenDayMeeting();

            BeanUtils.copyProperties(meeting, sevenDayMeeting);

            sevenDayMeeting.setRoomname(resultMap.get(meeting.getRoomid()));

            return sevenDayMeeting;
        }).collect(Collectors.toList());

        return listDto;


    }

    public List<CancelMeeting> getCancelMeeting() {

        List<Meeting> list = meetingMapper.getCancelMeeting();
        List<RoomDTO> listroomDTO = meetingRoomMapper.getAll();

        //得到Map<roomid,roomname>方便下面转换,添加对应的roomname
        Map<Integer, String> resultMap = listroomDTO.stream().collect(Collectors.toMap(
                RoomDTO::getRoomId, RoomDTO::getRoomName
        ));

        //转换Meeting为CancelMeeting,更好的在View展示,补充Meeting缺失的属性
        List<CancelMeeting> listDto = list.stream().map(meeting -> {

            CancelMeeting cancelMeeting = new CancelMeeting();

            BeanUtils.copyProperties(meeting, cancelMeeting);

            cancelMeeting.setRoomname(resultMap.get(meeting.getRoomid()));

            return cancelMeeting;
        }).collect(Collectors.toList());


        return listDto;
    }

    public Meeting getmeetingByid(Integer meetingid) {
        return meetingMapper.getmeetingByid(meetingid);

    }

    public List<MeetingDTO> getmeetingofmybookCanCancle(Integer employeeid) {
        return meetingMapper.getmeetingofmybook(employeeid);
    }

    public void cancelmeeting(Integer meetingid, String canceledreason) {
        meetingMapper.cancelmeeting(meetingid, canceledreason);
    }
}
