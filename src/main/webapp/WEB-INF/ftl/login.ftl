<!DOCTYPE html>
<html>
<head>
    <title>CoolMeeting会议管理系统</title>
    <link href="/styles/common.css" rel="stylesheet"/>
</head>
<body>
<#include './top.ftl'>
<div class="page-body">
    <#include 'leftMenu.ftl'>
    <div class="page-content">
        <div class="content-nav">
            登录
        </div>
        <form action="/doLogin" method="post">
            <fieldset>
                <legend>登录信息</legend>
                <table class="formtable" style="width:50%">
                    <tr>
                        <td>账号名:</td>
                        <td>
                            <input name="username" id="accountname" type="text"/>
                        </td>
                    </tr>
                    <tr>
                        <td>密码:</td>
                        <td>
                            <input name="password" id="new" type="password"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="command" colspan="3">
                            <a href="register">员工注册</a>
                            <input class="clickbutton" onclick="window.location.href='notifiactions.html';"
                                   type="submit"
                                   value="登录"/>
                            <input class="clickbutton" onclick="window.history.back();" type="button" value="返回"/>
                        </td>
                    </tr>
                </table>
                <div style="color: #ff0114">${error!''}</div>
            </fieldset>

        </form>
    </div>
</div>
<div class="page-footer">
    <hr/>
    更多问题，欢迎联系<a href="mailto:webmaster@eeg.com">管理员</a>
    <img alt="CoolMeeting" src="/images/footer.png"/>
</div>
</body>
</html>