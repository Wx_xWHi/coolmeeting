<div class="page-header">
    <div class="header-banner">
        <img src="/images/header.png" alt="CoolMeeting"/>
    </div>
    <div class="header-title">
        欢迎访问Cool-Meeting会议管理系统
    </div>
    <div class="header-quicklink">
        欢迎您，
        <#if currentuser??>
            <#if currentuser.role==2>
                <strong>${currentuser.employeename!''}-admin</strong>
            <#else >
                <strong>${currentuser.employeename!''}</strong>
            </#if>

        </#if>
        <a href="/logout">[退出]</a>
        <a href="changepassword.ftl">[修改密码]</a>
    </div>
</div>